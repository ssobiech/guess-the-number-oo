package pl.sobiech;

import java.text.ParseException;

public class Main {

    public static void main(String[] args) throws ParseException {
        new Game().run();
    }
}
