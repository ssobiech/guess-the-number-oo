package pl.sobiech.range;

public class FloatingRange extends Range {

    public FloatingRange(Number start, Number end) {
        super(start, end);
    }

    @Override
    public String toString() {
        return String.format("You can choose a number from between %.2f and %.2f", start.doubleValue(), end.doubleValue());
    }
}
