package pl.sobiech.range;

public class DecimalRange extends Range {

    public DecimalRange(Number start, Number end) {
        super(start, end);
    }

    @Override
    public String toString() {
        return String.format("You can choose a number from between %d and %d", start.intValue(), end.intValue());
    }
}
