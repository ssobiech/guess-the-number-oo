package pl.sobiech.range;

import pl.sobiech.UI;

import java.text.ParseException;

public class Range {

    private static final int DEFAULT_START_RANGE = 1;
    private static final int DEFAULT_END_RANGE = 100;

    protected Number start = DEFAULT_START_RANGE;
    protected Number end = DEFAULT_END_RANGE;
    private final UI ui;

    public Range() {
        this.ui = new UI();
    }

    public Range(Number start, Number end) {
        this();
        this.start = start;
        this.end = end;
    }

    public Number getStart() {
        return start;
    }

    public Number getEnd() {
        return end;
    }

    public Range createRange() throws ParseException {
        Range range;
        readRange();

        final boolean rangeIsFloating = this.start.doubleValue() % 1 > 0 && this.end.doubleValue() % 1 > 0;
        final boolean rangeIsDecimal = this.start.doubleValue() % 1 == 0 && this.end.doubleValue() % 1 == 0;

        if (rangeIsFloating) {
            range = new FloatingRange(start, end);
        } else if (rangeIsDecimal) {
            range = new DecimalRange(start, end);
        } else {
            ui.out("Both start and end range must be either decimal or floating! " +
                    "Default range of 1 to 100 has been set.\n\n");
            range = new DecimalRange(DEFAULT_START_RANGE, DEFAULT_END_RANGE);
        }

        return range;
    }

    private void readRange() throws ParseException {
        String isRangedChanged = ui.readText("Would you like to change the range?(y/n):  ");
        if (isRangedChanged.equalsIgnoreCase("y")) {
            String startRangeMsg = "Choose start range: ";
            this.start = ui.readNumber(startRangeMsg);

            String endRangeMsg = "Choose end range: ";
            this.end = ui.readNumber(endRangeMsg);
        }
    }
}
