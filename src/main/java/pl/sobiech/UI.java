package pl.sobiech;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Scanner;
import java.util.function.Consumer;

public class UI {

    private static final String IS_NUMBER_PATTERN = "[0-9]+";
    private static final String IS_FLOAT_NUMBER_PATTERN = "[0-9]*\\.?[0-9]+";

    private final Consumer<Object> out;
    private final Scanner in;

    public UI() {
        out = System.out::print;
        in = new Scanner(System.in);
    }

    public void out(Object msg) {
        out.accept(msg);
    }

    void printEmptyLine() {
        out("\n\n");
    }

    void printWelcomeMsg() {
        out("Welcome to the 'GUESS THE NUMBER' game!\n\n");
    }

    void printLoseMsg() {
        out("You lose!\n\n");
    }

    void printNumberOfTriesMsg(double numberOfTries) {
        out("You have " + String.format("%.0f", numberOfTries) + " tries. Good luck!\n\n");
    }

    public String readText(String msg) {
        String text = "";
        boolean isText = false;

        while (!isText) {
            out(msg);
            text = in.next();

            if (text.matches(IS_NUMBER_PATTERN)) {
                out("No numbers allowed. Try again. \n\n");
            } else {
                isText = true;
            }
        }

        return text;
    }

    public Number readNumber(String msg) throws ParseException {
        Number number = 0;
        boolean isNumber = false;

        while (!isNumber) {
            out(msg);
            String text = in.next();

            if (text.matches(IS_FLOAT_NUMBER_PATTERN)) {
                number = NumberFormat.getNumberInstance().parse(text);
                isNumber = true;
            } else {
                out("This is not an number! Try again.\n\n");
            }
        }

        return number;
    }
}