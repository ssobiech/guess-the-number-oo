package pl.sobiech;

import java.util.HashMap;
import java.util.Map;

public enum Answer {

    TOO_LOW(-1, "Too low!"),
    CORRECT(0, "Correct! That’s the number!"),
    TOO_HIGH(1, "Too high!");

    private final int key;
    private final String message;
    private static final Map<Integer, Answer> byKey = new HashMap<>();

    static {
        for (Answer a: Answer.values()) {
            byKey.put(a.key, a);
        }
    }

    Answer(int key, String message) {
        this.key = key;
        this.message = message;
    }

    public int getKey() {
        return key;
    }

    public String getMessage() {
        return message;
    }

    public static Answer of(int key) {
        return byKey.get(key);
    }
}
