package pl.sobiech;

import pl.sobiech.number.ChosenNumber;

import java.text.ParseException;

public class Try {

    private final UI ui;

    public Try(UI ui) {
        this.ui = ui;
    }

    public Answer execute(Number generatedRandomNumber) throws ParseException {
        Answer result;

        final Number number = ui.readNumber("Choose number: ");
        ChosenNumber chosenNumber = new ChosenNumber(number);
        int answerKey = chosenNumber.compareTo(generatedRandomNumber);
        result = Answer.of(answerKey);

        ui.out(result.getMessage());
        ui.printEmptyLine();

        return result;
    }
}
