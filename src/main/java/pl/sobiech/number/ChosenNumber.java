package pl.sobiech.number;

import java.util.Objects;

public class ChosenNumber extends BaseNumber implements Comparable<Number> {

    public ChosenNumber(Number number) {
        super(number);
    }

    @Override
    public int compareTo(Number o) {
        int result;
        if(o.doubleValue() % 1 > 0) {
            final double roundedFloatingNumber = Math.round(o.doubleValue() * 100) / 100.0;
            result = Double.compare(this.number.doubleValue(), roundedFloatingNumber);
        } else {
            result = Integer.compare(this.number.intValue(), o.intValue());
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChosenNumber that = (ChosenNumber) o;
        return Objects.equals(number, that.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
