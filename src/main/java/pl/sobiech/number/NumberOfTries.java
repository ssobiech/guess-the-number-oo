package pl.sobiech.number;

public class NumberOfTries {

    private static final double RANGE_FACTOR = 0.05;
    private static final int DEFAULT_NUMBER_OF_TRIES_FOR_FLOATS = 10;

    public int calculate(Number endRange) {
        final int number;
        final double remainder = endRange.doubleValue() % 1;

        if (remainder > 0 && remainder < 1) {
            number = DEFAULT_NUMBER_OF_TRIES_FOR_FLOATS;
        } else {
            number = (int) (endRange.doubleValue() * RANGE_FACTOR);
        }

        return number;
    }
}
