package pl.sobiech.number;

import pl.sobiech.number.strategy.RandomNumberGenerationStrategy;
import pl.sobiech.range.Range;

public class RandomNumber extends BaseNumber {

    public RandomNumber(Number number) {
        super(number);
    }

    public static RandomNumber generate(RandomNumberGenerationStrategy strategy, Range range) {
        if (strategy == null) {
            throw new IllegalArgumentException("Strategy cannot be null");
        }
        if (range == null) {
            throw new IllegalArgumentException("Range cannot be null");
        }
        return strategy.generate(range.getStart(), range.getEnd());
    }
}
