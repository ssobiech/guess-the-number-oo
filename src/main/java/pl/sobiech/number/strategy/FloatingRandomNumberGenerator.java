package pl.sobiech.number.strategy;

import pl.sobiech.number.RandomNumber;

import java.util.concurrent.ThreadLocalRandom;

public class FloatingRandomNumberGenerator implements RandomNumberGenerationStrategy {

    @Override
    public RandomNumber generate(Number floor, Number ceil) {
        return new RandomNumber(ThreadLocalRandom.current().nextDouble(floor.doubleValue(), ceil.doubleValue() + 1.0));
    }
}
