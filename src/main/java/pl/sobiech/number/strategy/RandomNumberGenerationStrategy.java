package pl.sobiech.number.strategy;

import pl.sobiech.number.RandomNumber;

public interface RandomNumberGenerationStrategy {

    RandomNumber generate(Number floor, Number ceil);
}
