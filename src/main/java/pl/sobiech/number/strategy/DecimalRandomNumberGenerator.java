package pl.sobiech.number.strategy;

import pl.sobiech.number.RandomNumber;

import java.util.concurrent.ThreadLocalRandom;

public class DecimalRandomNumberGenerator implements RandomNumberGenerationStrategy {

    @Override
    public RandomNumber generate(Number floor, Number ceil) {
        return new RandomNumber(ThreadLocalRandom.current().nextInt(floor.intValue(), ceil.intValue() + 1));
    }
}
