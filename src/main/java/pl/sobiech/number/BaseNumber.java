package pl.sobiech.number;

public abstract class BaseNumber extends Number {

    protected Number number;

    protected BaseNumber(Number number) {
        this.number = number;
    }

    public Number getNumber() {
        return number;
    }

    @Override
    public int intValue() {
        return number.intValue();
    }

    @Override
    public long longValue() {
        return number.longValue();
    }

    @Override
    public float floatValue() {
        return number.floatValue();
    }

    @Override
    public double doubleValue() {
        return number.doubleValue();
    }
}
