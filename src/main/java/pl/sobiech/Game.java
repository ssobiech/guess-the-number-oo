package pl.sobiech;

import pl.sobiech.number.*;
import pl.sobiech.number.strategy.DecimalRandomNumberGenerator;
import pl.sobiech.number.strategy.FloatingRandomNumberGenerator;
import pl.sobiech.range.Range;

import java.text.ParseException;

public class Game {

    private final UI ui;
    private Range range;

    public Game() {
        this.ui = new UI();
        this.range = new Range();
    }

    public void run() throws ParseException {
        ui.printWelcomeMsg();

        Range changedRange = range.createRange();
        RandomNumber generatedRandomNumber = generateRandomNumber(changedRange);
        int calculatedNumberOfTries = new NumberOfTries().calculate(changedRange.getEnd());

        ui.out(changedRange.toString());
        ui.printEmptyLine();
        ui.printNumberOfTriesMsg(calculatedNumberOfTries);

        int currentNumberOfTries = 0;
        Answer result;

        do {
            result = new Try(ui).execute(generatedRandomNumber.getNumber());
            currentNumberOfTries++;
        } while (result.getKey() != 0 && currentNumberOfTries < calculatedNumberOfTries);

        if (result.getKey() != 0) {
            ui.printLoseMsg();
        }
    }

    private RandomNumber generateRandomNumber(Range changedRange) {
        RandomNumber generatedRandomNumber;
        if (changedRange.getStart().doubleValue() % 1 > 0) {
            generatedRandomNumber = RandomNumber.generate(new FloatingRandomNumberGenerator(), changedRange);
        } else {
            generatedRandomNumber = RandomNumber.generate(new DecimalRandomNumberGenerator(), changedRange);
        }

        return generatedRandomNumber;
    }
}
